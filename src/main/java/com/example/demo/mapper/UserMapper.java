package com.example.demo.mapper;

import com.example.demo.dto.UserDto;
import com.example.demo.model.ShoppingCart;
import com.example.demo.model.User;
import com.example.demo.model.WishList;
import com.example.demo.model.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User map(UserDto userDto) {
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setAddress(userDto.getAddress());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setRole(Role.valueOf(userDto.getRole()));

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUser(user);
        user.setShoppingCart(shoppingCart);

        WishList wishList = new WishList();
        wishList.setUser(user);
        user.setWishList(wishList);

        return user;
    }
}
