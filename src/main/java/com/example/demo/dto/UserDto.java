package com.example.demo.dto;

import com.example.demo.model.enums.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@ToString
public class UserDto {

    private String name;
    private String email;
    private String password;
    private String role;
    private String phoneNumber;
    private String address;
}
