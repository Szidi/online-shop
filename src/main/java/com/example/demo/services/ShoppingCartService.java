package com.example.demo.services;

import com.example.demo.dto.PriceSummaryDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityDto;
import com.example.demo.dto.QuantityPriceDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.model.ShoppingCart;
import com.example.demo.model.User;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;


    public boolean addProduct(String productId, QuantityDto quantityDto, String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        Optional<Product> productOptional = productRepository.findById(Integer.valueOf(productId));
        if (productOptional.isEmpty()) {
            return false;
        }
        Product product = productOptional.get();
        Integer quantity = Integer.valueOf(quantityDto.getQuantity());
        if (product.getStock().getQuantity() < quantity) {
            return false;
        }
        for (int index = 0; index < quantity; index++) {
            shoppingCart.addProduct(product);
        }
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    public Map<ProductDto, QuantityPriceDto> retrieveShoppingCartContent(String userEmail) {

        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        List<Product> productList = shoppingCart.getProducts();
        Map<ProductDto, QuantityPriceDto> shoppingCartContent = new LinkedHashMap<>();

        for (Product product : productList) {
            ProductDto productDto = productMapper.mapProduct(product);
            if (shoppingCartContent.containsKey(productDto)) {
                QuantityPriceDto quantityPriceDto = shoppingCartContent.get(productDto);
                Integer quantity = quantityPriceDto.getQuantity() + 1;
                quantityPriceDto.setQuantity(quantity + 1);
                Integer totalPrice = quantityPriceDto.getTotalPrice();
                quantityPriceDto.setTotalPrice(totalPrice + Integer.parseInt(productDto.getPrice()));
            } else {
                QuantityPriceDto quantityPriceDto = new QuantityPriceDto();
                quantityPriceDto.setTotalPrice(Integer.valueOf(productDto.getPrice()));
                quantityPriceDto.setQuantity(1);
                shoppingCartContent.put(productDto, quantityPriceDto);
            }
        }
        return shoppingCartContent;
    }

    public PriceSummaryDto computePriceSummaryDto(Map<ProductDto, QuantityPriceDto> productMap) {

        Integer subtotalPrice = productMap.values().stream()
                .map(QuantityPriceDto::getTotalPrice)
                .mapToInt(Integer::intValue)
                .sum();

        int shipping = 50;
        int total = subtotalPrice + shipping;
        PriceSummaryDto priceSummaryDto = new PriceSummaryDto(subtotalPrice, shipping, total);
        return priceSummaryDto;
    }
}
