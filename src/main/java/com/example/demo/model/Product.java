package com.example.demo.model;

import com.example.demo.model.enums.Category;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue
    private Integer productId;
    private String name;
    private Integer price;

    @Enumerated(value = EnumType.STRING)
    private Category category;
    private String description;

    @Lob
    private byte[] image;

    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "shoppingCartId"), inverseJoinColumns = @JoinColumn(name = "wishlistId"))
    private List<WishList> wishLists;

    @ManyToMany(mappedBy = "products", cascade = CascadeType.ALL)
    private List<ShoppingCart> shoppingCarts;

    @ManyToMany(mappedBy = "products")
    private List<Order> orders;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private Stock stock;

    public void decreaseQuantityByOne() {
        stock.decreaseQuantityByOne();
    }
}
