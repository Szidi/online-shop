package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class WishList {
    @Id
    @GeneratedValue
    private Integer wishlistId;
    @OneToOne
    private User user;
    @ManyToMany(mappedBy = "wishLists")
    private List<Product> products;
}
